module gitlab.com/tanqhnguyen/gohtml

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/stretchr/testify v1.5.1
)
