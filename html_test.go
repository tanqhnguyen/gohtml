package gohtml

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseSimpleStruct(t *testing.T) {
	assert := assert.New(t)
	type testStruct struct {
		Title   string `html:"#entry > .title"`
		Name    string `html:"#entry > .name"`
		Country string `html:"#entry > .country"`
		DOB     int    `html:"#entry > .dob"`
	}
	data, _ := ioutil.ReadFile("testdata/single_entry.html")

	htmlParser := NewParser()

	var ts testStruct
	err := htmlParser.Parse(data, &ts)

	assert.Nil(err)
	assert.EqualValues(testStruct{
		Title:   "Mr",
		Name:    "Tan Nguyen",
		Country: "Vietnam",
		DOB:     1989,
	}, ts)
}

func TestParseNestedStruct(t *testing.T) {
	assert := assert.New(t)
	type nestedStruct struct {
		Title   string `html:".title"`
		Name    string `html:".name:nth-of-type(2)"`
		Country string `html:".country"`
	}

	type testStruct struct {
		Entry nestedStruct `html:"#entry"`
	}

	data, _ := ioutil.ReadFile("testdata/single_entry.html")

	htmlParser := NewParser()

	var ts testStruct
	err := htmlParser.Parse(data, &ts)

	assert.Nil(err)
	assert.EqualValues(testStruct{
		Entry: nestedStruct{
			Title:   "Mr",
			Name:    "Tan Nguyen",
			Country: "Vietnam",
		},
	}, ts)
}

func TestParseDeeplyNestedStruct(t *testing.T) {
	assert := assert.New(t)

	type anotherNestedStruct struct {
		Name string `html:".name"`
	}

	type nestedStruct struct {
		Title   string              `html:".title"`
		Name    string              `html:".name:nth-of-type(2)"`
		Country string              `html:".country"`
		Game    anotherNestedStruct `html:".game"`
	}

	type testStruct struct {
		Entry nestedStruct `html:"#entry"`
	}

	data, _ := ioutil.ReadFile("testdata/single_entry.html")

	htmlParser := NewParser()

	var ts testStruct
	err := htmlParser.Parse(data, &ts)

	assert.Nil(err)
	assert.EqualValues(testStruct{
		Entry: nestedStruct{
			Title:   "Mr",
			Name:    "Tan Nguyen",
			Country: "Vietnam",
			Game: anotherNestedStruct{
				Name: "Monster hunter",
			},
		},
	}, ts)
}

func TestParseStructWithAnotherStructSlice(t *testing.T) {
	assert := assert.New(t)

	type nestedStruct struct {
		Name   string  `html:".name"`
		Rating float32 `html:".rating"`
	}

	type testStruct struct {
		Games []nestedStruct `html:".game"`
	}

	data, _ := ioutil.ReadFile("testdata/multiple_entries.html")

	htmlParser := NewParser()

	var ts testStruct
	err := htmlParser.Parse(data, &ts)

	assert.Nil(err)

	assert.EqualValues(testStruct{
		Games: []nestedStruct{{
			Name:   "Monster hunter",
			Rating: 9.8,
		}, {
			Name:   "Counter Strike",
			Rating: 9.2,
		},
		},
	}, ts)
}

func TestParseStructWithAnotherStringSlice(t *testing.T) {
	assert := assert.New(t)

	type testStruct struct {
		Names  []string  `html:".name"`
		Ints   []int     `html:".int"`
		Floats []float64 `html:".float"`
	}

	data, _ := ioutil.ReadFile("testdata/multiple_values.html")

	htmlParser := NewParser()

	var ts testStruct
	err := htmlParser.Parse(data, &ts)

	assert.Nil(err)

	assert.EqualValues(testStruct{
		Names:  []string{"Monster hunter", "Counter Strike"},
		Ints:   []int{1, 2, 3},
		Floats: []float64{1.1, 2.2, 3.3},
	}, ts)
}

func TestParseAttribute(t *testing.T) {
	assert := assert.New(t)

	type dobStruct struct {
		Value int `html:"value,attr"`
	}

	type testStruct struct {
		Title   string    `html:"#entry > .title"`
		Name    string    `html:"#entry > .name"`
		Country string    `html:"#entry > .country"`
		DOB     dobStruct `html:"#entry > .dob"`
	}
	data, _ := ioutil.ReadFile("testdata/attribute.html")

	htmlParser := NewParser()

	var ts testStruct
	err := htmlParser.Parse(data, &ts)

	assert.Nil(err)
	assert.EqualValues(testStruct{
		Title:   "Mr",
		Name:    "Tan Nguyen",
		Country: "Vietnam",
		DOB: dobStruct{
			Value: 1989,
		},
	}, ts)
}
